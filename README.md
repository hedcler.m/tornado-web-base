# Tornado Web - Base structure

Base project structure to work with Tornado Web framework.

# Structure:

		/
			/config
				settings.py // Application Settings
				urls.py // Application Routes to Handlers
			/handlers // All handlers here
				default.py // Default handler
			/library // Your project library
			/logs // Application Logs
			/templates // Tornado templates path
				/static // Tornado static path
			/tests // All tests
			server.py // Server
